module gitlab.com/verraedt/powmon

go 1.14

require (
	github.com/peterverraedt/go-pigpio v0.0.0-20211219174350-974faccb45f8
	github.com/prometheus/client_golang v0.9.3
	github.com/ryotarai/prometheus-query v0.0.0-20181009101647-ea75bc6bc1b1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.3
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	google.golang.org/appengine v1.6.1 // indirect
)
