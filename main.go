package main

import (
	"log"
	"time"

	"github.com/peterverraedt/go-pigpio"
	"github.com/spf13/cobra"
	"gitlab.com/verraedt/powmon/pkg/monitor"
)

func main() {
	var config monitor.Config

	rootCmd := &cobra.Command{
		Use:   "powmon",
		Short: "Power monitor and manager",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			p, err := monitor.New(&config)
			if err != nil {
				log.Fatal(err)
			}

			err = p.Run()
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	rootCmd.Flags().StringVar(&config.PrometheusURL, "prometheus", "http://prometheus.service.consul:9090", "Protocol and address of the prometheus service")
	rootCmd.Flags().StringVar(&config.EbusdAddress, "ebusd", "pi01-ebusd.service.consul:8888", "Address of the ebusd service")
	rootCmd.Flags().StringVar(&config.FluviusID, "fluvius-id", "3153414733313030303432313135", "Identifier of Fluvius counter")
	rootCmd.Flags().DurationVar(&config.Range, "range", 5*time.Minute, "Range of results to query from prometheus, by default last 5 minutes are queried")
	rootCmd.Flags().DurationVar(&config.Interval, "interval", 15*time.Second, "Interval between results to query from prometheus, by default 15 seconds")
	rootCmd.Flags().DurationVar(&config.SensorInterval, "sensor-interval", 62*time.Second, "Period to expect at least one temperature sensor broadcast from the sensor")
	rootCmd.Flags().DurationVar(&config.MeteoInterval, "meteo-interval", 2*time.Minute, "Period to refresh meteo information")
	rootCmd.Flags().StringVar(&config.PigpioAddr, "pi", "pi03.lan40.edgemax2.net.verraedt.be:8888", "Address of pigpio, in host:port format.")
	rootCmd.Flags().Uint32Var(&config.VentilationGPIO, "pin", uint32(pigpio.GPIO17), "Ventilation GPIO pin number")
	rootCmd.Flags().BoolVarP(&config.Debug, "verbose", "v", false, "Verbose output.")
	rootCmd.Flags().IntVarP(&config.Port, "port", "p", 9000, "Port for HTTP server exporting metrics.")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
