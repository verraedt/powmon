package ebusd

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// EbusdTimeout is timeout for communication.
const EbusdTimeout = 5 * time.Second

type Client struct {
	Address string
}

// Command to send to ebusd.
func (c *Client) Command(command string) (string, error) {
	conn, err := net.Dial("tcp", c.Address)
	if err != nil {
		return "", err
	}

	err = conn.SetDeadline(time.Now().Add(EbusdTimeout))
	if err != nil {
		return "", err
	}

	fmt.Fprintf(conn, "%s\n", command)

	answer, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		return "", err
	}

	return strings.TrimSuffix(answer, "\n"), nil
}

// ErrEbusdSetFailed is returned in case an unexpected answer is given.
var ErrEbusdSetFailed = errors.New("ebusd set failed")

// Set sets a ebusd parameter.
func (c *Client) Set(circuit, param string, value float64) error {
	answer, err := c.Command(fmt.Sprintf("w -c %s %s %f", circuit, param, value))
	if err != nil {
		return err
	}

	if answer != "done" {
		return fmt.Errorf("%w: %s", ErrEbusdSetFailed, answer)
	}

	return nil
}

// Get reads a ebusd parameter.
func (c *Client) Get(circuit, param string) (float64, error) {
	answer, err := c.Command(fmt.Sprintf("r -c %s %s", circuit, param))
	if err != nil {
		return 0, err
	}

	return strconv.ParseFloat(answer, 64)
}

// ErrNoValue is returned by EbusdLastUpdate is the specified parameter is not found.
var ErrNoValue = errors.New("no ebusd value found")

// EbusdLastUpdate reads information about the last update.
func (c *Client) LastUpdate(circuit, param string) (time.Time, float64, error) {
	answer, err := c.Command(fmt.Sprintf("find -vvv -c %s %s", circuit, param))
	if err != nil {
		return time.Time{}, 0, err
	}

	// broadcast outsidetemp = temp2=16.602 °C [Temperatur] [ZZ=fe, lastup=2021-04-27 11:17:41, passive read]
	prefix := regexp.QuoteMeta(fmt.Sprintf("%s %s = ", circuit, param))

	re, err := regexp.Compile(prefix + `.*=([0-9.]*) .* \[.*\] \[ZZ=.*, lastup=([0-9- :]*), .*\]`)
	if err != nil {
		return time.Time{}, 0, err
	}

	for _, line := range strings.Split(answer, "\n") {
		match := re.FindStringSubmatch(line)

		if len(match) > 0 {
			ts, err := time.Parse("2006-01-02 15:04:05", match[2])
			if err != nil {
				return time.Time{}, 0, err
			}

			val, err := strconv.ParseFloat(match[1], 64)

			return ts, val, err
		}
	}

	return time.Time{}, 0, ErrNoValue
}

// Broadcast sends a broadcast message using hex.
func (c *Client) Broadcast(primary, secondary byte, data ...byte) error {
	hex := fmt.Sprintf("fe%02x%02x%02x", primary, secondary, len(data))

	for _, b := range data {
		hex += fmt.Sprintf("%02x", b)
	}

	logrus.Debugf("Sending hex %s\n", hex)

	answer, err := c.Command(fmt.Sprintf("hex %s", hex))
	if err != nil {
		return err
	}

	if answer != "done broadcast" {
		return fmt.Errorf("%w: %s", ErrEbusdSetFailed, answer)
	}

	return nil
}
