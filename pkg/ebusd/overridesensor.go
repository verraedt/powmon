package ebusd

import (
	"math"

	"github.com/sirupsen/logrus"
)

// SetOutsideTemperature sets the outside temperature.
func (c *Client) SetOutsideTemperature(temp float64) error {
	logrus.Infof("Current outside temperature: %.1f°C", temp)

	a, b := floatToHex(temp)

	return c.Broadcast(0xb5, 0x16, 0x1, a, b)
}

func floatToHex(value float64) (byte, byte) {
	val := int(math.Round(value * 256))

	if val > 0xffff {
		val = 0xffff
	}

	return byte(val % 0x100), byte(val / 0x100)
}
