package leuvencool

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

// URLs to query meteo information from
const (
	MeteoKruidtuin = "https://www.meteo.be/services/wow/ajax/getLatestObservation.php?id=de6a44f1-cfba-ea11-bf21-0003ff59a56f"
	MeteoSluispark = "https://www.meteo.be/services/wow/ajax/getLatestObservation.php?id=9ed0ff9b-2013-e811-90cf-000d3ab1c4d1"
)

// Meteo is returned by the page.
type Meteo struct {
	Time    time.Time `json:"timestamp"`
	Primary `json:"primary"`
}

// Primary data.
type Primary struct {
	Temperature   float64 `json:"dt" gauge:"powmon_meteo_temperature" description:"Temperature"`
	Humidity      float64 `json:"dh" gauge:"powmon_meteo_humidity" description:"Humidity"`
	Pressure      float64 `json:"dm" gauge:"powmon_meteo_pressure" description:"Pressure"`
	WindSpeed     float64 `json:"dws" gauge:"powmon_meteo_wind_speed" description:"Wind speed"`
	WindDirection float64 `json:"dwd" gauge:"powmon_meteo_wind_direction" description:"Wind direction"`
	Rain          float64 `json:"dr" gauge:"powmon_meteo_rain" description:"Rainfall"`
}

// ErrNoResult is returned if the URL is empty.
var ErrNoResult = errors.New("http get did not retrieve results")

// ErrTooOld is returned if the found temperature measurement is older than 10 minutes.
var ErrTooOld = errors.New("result is too old")

// ReadMeteoURL fetches meteo information from a given URL.
func ReadMeteoURL(url string) (Meteo, error) {
	result := Meteo{}

	resp, err := http.Get(url) // nolint:gosec
	if err != nil {
		return result, err
	}

	defer resp.Body.Close()

	payload, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(payload, &result)
	if err != nil {
		return result, err
	}

	if time.Since(result.Time) > time.Hour {
		return result, ErrTooOld
	}

	return result, nil
}
