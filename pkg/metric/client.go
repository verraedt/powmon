package metric

import (
	"errors"
	"fmt"
	"time"

	prometheus "github.com/ryotarai/prometheus-query/client"
	"github.com/sirupsen/logrus"
)

type Client struct {
	PrometheusClient *prometheus.Client
	FluviusID        string
	Range            time.Duration
	Interval         time.Duration
}

// ErrPrometheusNoSuccess is returned if prometheus query failed logically.
var ErrPrometheusNoSuccess = errors.New("prometheus did not return success status")

// ErrPrometheusTooManyResults is returned if prometheus returns more than one result.
var ErrPrometheusTooManyResults = errors.New("prometheus returned more than one result vector")

type Value struct {
	Value float64
	Time  time.Time
}

// Query a prometheus query.
func (c *Client) Query(query string, now time.Time) ([]Value, error) {
	result, err := c.PrometheusClient.QueryRange(query, now.Add(-1*c.Range), now, c.Interval)
	if err != nil {
		return nil, err
	}

	if result.Status != "success" {
		return nil, fmt.Errorf("%w: %s (%s)", ErrPrometheusNoSuccess, result.Status, query)
	}

	if result.Data == nil {
		return nil, fmt.Errorf("%w: data is empty (%s)", ErrPrometheusNoSuccess, query)
	}

	if len(result.Data.Result) != 1 {
		return nil, fmt.Errorf("%w: %d (%s)", ErrPrometheusTooManyResults, len(result.Data.Result), query)
	}

	floats := []Value{}

	for _, value := range result.Data.Result[0].Values {
		float, err := value.Value()
		if err != nil {
			return nil, fmt.Errorf("parse error: %w", err)
		}

		logrus.Debugf("query %s yields value at %s: %f", query, value.Time().String(), float)

		floats = append(floats, Value{float, value.Time()})
	}

	return floats, nil
}

func (c *Client) Metrics(now time.Time) ([]Metric, error) {
	solarSMA, err := c.Query(`sma_ac_power{job="sma-exporter"}`, now)
	if err != nil {
		return nil, err
	}

	delivered, err := c.Query(fmt.Sprintf(`fluvius_electricity_currently_delivered_avg_15{ean="%s"}*1000`, c.FluviusID), now)
	if err != nil {
		return nil, err
	}

	returned, err := c.Query(fmt.Sprintf(`fluvius_electricity_currently_returned_avg_15{ean="%s"}*1000`, c.FluviusID), now)
	if err != nil {
		return nil, err
	}

	total, err := c.Query(`avg(power_power_avg_15{sensor="2"})+avg(power_power_avg_15{sensor="3"})+avg(power_power_avg_15{sensor="4"})`, now)
	if err != nil {
		return nil, err
	}

	heating, err := c.Query(`0.95*(avg(power_power_avg_15{sensor="5"})+avg(power_power_avg_15{sensor="10"})+avg(power_power_avg_15{sensor="11"}))`, now)
	if err != nil {
		return nil, err
	}

	solar, err := c.Query(`avg(power_power_avg_15{sensor="6"})`, now)
	if err != nil {
		return nil, err
	}

	roomTemp, err := c.Query(`ebusd_z1_room_temp`, now)
	if err != nil {
		return nil, err
	}

	/*co2, err := p.Query(`gas_ppm`, now)
	if err != nil {
		return nil, err
	}*/

	if err := checkSizes(solarSMA, delivered, returned, heating, solar, total, roomTemp); err != nil {
		return nil, err
	}

	result := make([]Metric, len(solar))

	for i := range result {
		result[i].Delivered = delivered[i].Value
		result[i].Returned = returned[i].Value
		result[i].Total = total[i].Value
		result[i].SolarSMA = solarSMA[i].Value
		result[i].Solar = solar[i].Value
		result[i].Heating = heating[i].Value
		result[i].RoomTemperature = roomTemp[i].Value
		//result[i].CO2 = co2[i].Value
	}

	return result, nil
}

// ErrNoMatchingSizes is returned if statistics of different lengths are retrieved.
var ErrNoMatchingSizes = errors.New("lengths do not match")

// checks whether a list of slices has equal lengths
func checkSizes(slices ...[]Value) error {
	for i := 1; i < len(slices); i++ {
		if len(slices[0]) != len(slices[i]) {
			return fmt.Errorf("%w: slice 0 has length %d, slice %d has length %d", ErrNoMatchingSizes, len(slices[0]), i, len(slices[i]))
		}
	}

	return nil
}
