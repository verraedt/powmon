package metric

import "fmt"

// Metric is a statistic
type Metric struct {
	Delivered       float64
	Returned        float64
	Total           float64
	Solar           float64
	SolarSMA        float64
	Heating         float64
	RoomTemperature float64
	//CO2             float64
}

type Metrics []Metric

// NonHeatingConsumption calculates all consumption except for heating.
func (m Metric) NonHeatingConsumption() float64 {
	value := m.Delivered + m.Solar - m.Returned - m.Heating

	if value < 0 {
		// Never believe a negative value
		return 0
	}

	return value
}

// String representation of stat.
func (m *Metric) String() string {
	return fmt.Sprintf("delivered: %.2f, returned: %.2f (total: %.2f, delta: %.2f), solar: %.2f (SMA: %.0f), heating: %.2f, non-heating: %.2f",
		m.Delivered,
		m.Returned,
		m.Total,
		m.Delivered-m.Returned-m.Total,
		m.Solar,
		m.SolarSMA,
		m.Heating,
		m.NonHeatingConsumption(),
	)
}

func (metrics Metrics) Average() Metric {
	return metrics.Aggregate(func(tot, val float64, _ int) float64 {
		return tot + (val / float64(len(metrics)))
	})
}

func (metrics Metrics) Maximum() Metric {
	return metrics.Aggregate(func(tot, val float64, i int) float64 {
		if i == 0 || val > tot {
			return val
		}

		return tot
	})
}

func (metrics Metrics) Minimum() Metric {
	return metrics.Aggregate(func(tot, val float64, i int) float64 {
		if i == 0 || val < tot {
			return val
		}

		return tot
	})
}

func (metrics Metrics) Last(length int) Metrics {
	if len(metrics) < length {
		length = len(metrics)
	}

	return metrics[len(metrics)-length:]
}

func (metrics Metrics) Aggregate(agg func(float64, float64, int) float64) Metric {
	var result Metric

	for i, metric := range metrics {
		result.Delivered = agg(result.Delivered, metric.Delivered, i)
		result.Returned = agg(result.Returned, metric.Returned, i)
		result.Total = agg(result.Total, metric.Total, i)
		result.Solar = agg(result.Solar, metric.Solar, i)
		result.SolarSMA = agg(result.SolarSMA, metric.SolarSMA, i)
		result.Heating = agg(result.Heating, metric.Heating, i)
		result.RoomTemperature = agg(result.RoomTemperature, metric.RoomTemperature, i)
	}

	return result
}

// MinMax takes the maximum values of groups of M metrics,
// and takes the minimum value of m of such groups.
func (metrics Metrics) MinMax(m, M int) Metric {
	// Take the maximum value of m metrics
	maximums := make(Metrics, len(metrics)-m+1)

	for first := 0; first < len(maximums); first++ {
		maximums[first] = metrics[first : first+m].Maximum()
	}

	// Take the minimum value of the last M of those maximums
	return maximums.Last(M).Minimum()
}

func Maximum(s []float64) float64 {
	if len(s) == 0 {
		return 0
	}

	t := s[0]

	for _, v := range s[1:] {
		if t < v {
			t = v
		}
	}

	return t
}

func Minimum(s []float64) float64 {
	if len(s) == 0 {
		return 0
	}

	t := s[0]

	for _, v := range s[1:] {
		if t > v {
			t = v
		}
	}

	return t
}

func MinMax(s []float64, m, M int) float64 {
	// Take the maximum value of m metrics
	maximums := make([]float64, len(s)-m+1)

	for first := 0; first < len(maximums); first++ {
		maximums[first] = Maximum(s[first : first+m])
	}

	// Take the minimum value of the last M of those maximums
	return Minimum(Last(maximums, M))
}

func Last(s []float64, n int) []float64 {
	if n > len(s) {
		n = len(s)
	}

	return s[len(s)-n:]
}

// Map metrics to a float64 slice by using a mapping function.
func (metrics Metrics) Map(mapping func(Metric) float64) []float64 {
	results := []float64{}

	for _, m := range metrics {
		results = append(results, mapping(m))
	}

	return results
}

// OverrideSensor indicates whether the outside temperature sensor should be overwritten.
func (metrics Metrics) OverrideSensor() bool {
	return metrics.Average().Solar >= 600
}

// Temperature to set.
func (metrics Metrics) Temperature() float64 {
	// Avoid heating if power usage is above 1000 to avoid high peak usage
	if metrics.Last(4).Average().NonHeatingConsumption() > 1000 {
		return 16
	}

	return MinMax(metrics.Map(func(m Metric) float64 {
		if m.Heating < 0 {
			m.Heating = 0
		}

		nonHeatingConsumption := m.NonHeatingConsumption()

		if m.Heating > 200 {
			// Avoid flapping if heating is on and readings are a bit off
			nonHeatingConsumption -= 50
		}

		// Do nothing if 'free solar energy' is less than 600
		if m.Solar-nonHeatingConsumption < 600 {
			return 21
		}

		if m.Solar-nonHeatingConsumption < 1200 {
			if m.RoomTemperature < 21.25 {
				// Boost
				return 22
			}

			return 21.5
		}

		if m.RoomTemperature < 21.75 {
			// Boost
			return 23
		}

		return 22
	}), NumberOfSettingsThresholdDown, NumberOfSettingsThresholdUp)
}

// HotwaterTemperature to set.
func (metrics Metrics) HotwaterTemperature() float64 {
	// Avoid heating if power usage is above 1000 to avoid high peak usage
	if metrics.Last(4).Average().NonHeatingConsumption() > 1000 {
		return 35
	}

	return MinMax(metrics.Map(func(m Metric) float64 {
		nonHeatingConsumption := m.NonHeatingConsumption()

		if m.Heating > 200 {
			// Avoid flapping if heating is on and readings are a bit off
			nonHeatingConsumption -= 50
		}

		if m.Solar-nonHeatingConsumption < 600 {
			return 50
		}

		if m.Solar-nonHeatingConsumption < 1700 {
			return 55
		}

		return 60
	}), NumberOfSettingsThresholdDown, NumberOfSettingsThresholdUp)
}

// NumberOfSettingsThresholdUp avoids flapping too fast.
// Moving to higher settings after seeing good conditions for 4 minutes
const NumberOfSettingsThresholdUp = 16

// NumberOfSettingsThresholdDown avoids flapping too fast.
// Moving to lower settings after seeing bad conditions for 75 seconds
const NumberOfSettingsThresholdDown = 5
