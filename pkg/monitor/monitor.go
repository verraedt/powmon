package monitor

import (
	"fmt"
	"net/http"
	"time"

	"github.com/peterverraedt/go-pigpio"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	prometheusclient "github.com/ryotarai/prometheus-query/client"
	"gitlab.com/verraedt/powmon/pkg/ebusd"
	"gitlab.com/verraedt/powmon/pkg/leuvencool"
	"gitlab.com/verraedt/powmon/pkg/metric"
	"gitlab.com/verraedt/powmon/pkg/purpleair"

	log "github.com/sirupsen/logrus"
)

// Config for PowMon.
type Config struct {
	PrometheusURL   string
	EbusdAddress    string
	FluviusID       string
	Range           time.Duration
	Interval        time.Duration
	SensorInterval  time.Duration
	MeteoInterval   time.Duration
	PigpioAddr      string
	VentilationGPIO uint32
	Debug           bool
	Port            int
}

// Monitor object.
type Monitor struct {
	*Config

	Metrics *metric.Client
	Ebusd   *ebusd.Client
	Gauges  map[string]*prometheus.GaugeVec

	overrideSensor bool
	maxVentilation bool
	meteo          leuvencool.Meteo
}

// New creates a Monitor.
func New(config *Config) (*Monitor, error) {
	pclient, err := prometheusclient.NewClient(config.PrometheusURL)
	if err != nil {
		return nil, err
	}

	p := &Monitor{
		Config: config,
		Metrics: &metric.Client{
			PrometheusClient: pclient,
			FluviusID:        config.FluviusID,
			Range:            config.Range,
			Interval:         config.Interval,
		},
		Ebusd: &ebusd.Client{
			Address: config.EbusdAddress,
		},
		Gauges: map[string]*prometheus.GaugeVec{},
	}

	for k, v := range GaugesFor(leuvencool.Primary{}) {
		p.Gauges[k] = v
	}

	for k, v := range GaugesFor(purpleair.Sensor{}) {
		p.Gauges[k] = v
	}

	http.Handle("/metrics", promhttp.Handler())

	p.ReadMeteo()

	return p, err
}

// Run the monitor.
func (p *Monitor) Run() error {
	// Set debug level
	if p.Debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	// Always start overriding the sensor
	p.overrideSensor = true

	// Start HTTP server
	httpch := make(chan error)

	go func() {
		httpch <- http.ListenAndServe(fmt.Sprintf(":%d", p.Port), nil)
	}()

	// Timers
	refreshTicker := time.NewTicker(p.Interval)
	meteoTicker := time.NewTicker(p.MeteoInterval)

	for {
		select {
		case <-refreshTicker.C:
			err := p.Refresh()
			if err != nil {
				log.Error(err)
			}

		case <-meteoTicker.C:
			p.ReadMeteo()

		/*case <-sensorTimer.C:
		if !p.overrideSensor {
			continue
		}

		// Check the last broadcast of the sensor
		if ts, _, err := p.EbusdLastUpdate("broadcast", "outsidetemp"); err != nil {
			log.Error(err)

			sensorTimer.Reset(p.SensorInterval / 2)
		} else {
			next := p.SensorInterval - time.Since(ts)

			if next < time.Second {
				next = time.Second
			}

			sensorTimer.Reset(next)
		}

		if err := p.SetOutsideTemperature(); err != nil {
			log.Error(err)
		}*/

		case err := <-httpch:
			log.Fatal(err)
		}
	}
}

func (p *Monitor) ReadMeteo() {
	if kruidtuin, err := leuvencool.ReadMeteoURL(leuvencool.MeteoKruidtuin); err == nil {
		p.SetGaugeValues(kruidtuin.Primary, "Kruidtuin")

		// Update cache except pressure field
		kruidtuin.Pressure = p.meteo.Pressure
		p.meteo = kruidtuin
	} else {
		log.Errorf("Could not read meteo for Kruidtuin: %s", err)
	}

	if sluispark, err := leuvencool.ReadMeteoURL(leuvencool.MeteoSluispark); err == nil {
		p.SetGaugeValues(sluispark.Primary, "Sluispark")

		// Update cache pressure field
		p.meteo.Pressure = sluispark.Pressure
	} else {
		log.Errorf("Could not read meteo for Sluispark: %s", err)
	}

	if purpleair, err := purpleair.ReadPurpleAirURL(purpleair.PurpleAirSluispark); err == nil {
		p.SetGaugeValues(purpleair.Sensor, purpleair.Sensor.Label)
	} else {
		log.Errorf("Could not read purple air for Sluispark: %s", err)
	}
}

// Refresh the state.
func (p *Monitor) Refresh() error {
	status, err := p.Status()
	if err != nil {
		return err
	}

	// Log current metrics
	log.Infof(status.Metrics[len(status.Metrics)-1].String())

	// Determine desired settings
	settings := status.NextSettings()

	if status.Settings.Equal(&settings) {
		log.Infof("Settings: %s", settings.String())

		return nil
	}

	log.Infof("Settings: %s -> %s", status.Settings.String(), settings.String())

	return p.Apply(status.Settings, settings)
}

// Apply a status.
func (p *Monitor) Apply(current Settings, next Settings) error {
	if next.HotWaterTemperature != current.HotWaterTemperature {
		log.Infof("Setting Hot Water Temperature to: %.1f°C", next.HotWaterTemperature)

		if err := p.Ebusd.Set("700", "HwcTempDesired", next.HotWaterTemperature); err != nil {
			return err
		}
	}

	if next.HeatingTemperature != current.HeatingTemperature {
		log.Infof("Setting Heating Temperature to: %.1f°C", next.HeatingTemperature)

		if err := p.Ebusd.Set("700", "z1DayTemp", next.HeatingTemperature); err != nil {
			return err
		}
	}

	p.overrideSensor = next.OverrideSensor

	if p.maxVentilation != next.MaxVentilation {
		client, err := pigpio.New(p.PigpioAddr)
		if err != nil {
			return err
		}

		err = client.SetMode(pigpio.Pin(p.VentilationGPIO), pigpio.OUTPUT)
		if err != nil {
			return err
		}

		err = client.Write(pigpio.Pin(p.VentilationGPIO), !p.maxVentilation)
		if err != nil {
			return err
		}

		p.maxVentilation = next.MaxVentilation
	}

	return nil
}
