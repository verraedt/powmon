package monitor

import (
	"reflect"

	"github.com/prometheus/client_golang/prometheus"
)

func GaugesFor(field interface{}) map[string]*prometheus.GaugeVec {
	e := reflect.TypeOf(field)

	result := map[string]*prometheus.GaugeVec{}

	for i := 0; i < e.NumField(); i++ {
		gauge := e.Field(i).Tag.Get("gauge")
		if gauge == "" {
			continue
		}

		description := e.Field(i).Tag.Get("description")

		if description == "" {
			description = gauge
		}

		result[gauge] = prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: gauge,
			Help: description,
		}, []string{"location"})

		prometheus.MustRegister(result[gauge])
	}

	return result
}

func (p *Monitor) SetGaugeValues(values interface{}, location string) {
	v := reflect.ValueOf(values)
	e := reflect.TypeOf(values)

	for i := 0; i < e.NumField(); i++ {
		name := e.Field(i).Tag.Get("gauge")
		if name == "" {
			continue
		}

		gauge, ok := p.Gauges[name]
		if !ok {
			continue
		}

		value := v.Field(i).Float()

		gauge.WithLabelValues(location).Set(value)
	}
}
