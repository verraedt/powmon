package monitor

import (
	"fmt"
)

// Settings is a list of settings we can tweak and their values.
type Settings struct {
	HeatingTemperature  float64
	HotWaterTemperature float64
	OverrideSensor      bool
	MaxVentilation      bool
}

// Equal checks for equality.
func (s *Settings) Equal(o *Settings) bool {
	return s.HeatingTemperature == o.HeatingTemperature && s.HotWaterTemperature == o.HotWaterTemperature && s.OverrideSensor == o.OverrideSensor && s.MaxVentilation == o.MaxVentilation
}

// String for Settings.
func (s *Settings) String() string {
	return fmt.Sprintf("heating: %.1f°C, hot water: %.1f°C, override: %v, ventilation: %v", s.HeatingTemperature, s.HotWaterTemperature, s.OverrideSensor, s.MaxVentilation)
}
