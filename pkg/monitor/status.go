package monitor

import (
	"time"

	"gitlab.com/verraedt/powmon/pkg/leuvencool"
	"gitlab.com/verraedt/powmon/pkg/metric"
)

// Status is current status.
type Status struct {
	Time                       time.Time
	Metrics                    metric.Metrics
	CurrentRoomTemperature     float64
	CurrentHotWaterTemperature float64
	Meteo                      leuvencool.Meteo
	Settings
}

// Status gets metrics and current temperatures
func (p *Monitor) Status() (*Status, error) {
	var (
		status Status
		err    error
	)

	status.Time = time.Now()

	status.Metrics, err = p.Metrics.Metrics(status.Time)
	if err != nil {
		return nil, err
	}

	status.HeatingTemperature, err = p.Ebusd.Get("700", "z1DayTemp")
	if err != nil {
		return nil, err
	}

	status.HotWaterTemperature, err = p.Ebusd.Get("700", "HwcTempDesired")
	if err != nil {
		return nil, err
	}

	status.CurrentRoomTemperature, err = p.Ebusd.Get("700", "z1RoomTemp")
	if err != nil {
		return nil, err
	}

	status.CurrentHotWaterTemperature, err = p.Ebusd.Get("700", "HwcStorageTemp")
	if err != nil {
		return nil, err
	}

	status.OverrideSensor = p.overrideSensor
	status.MaxVentilation = p.maxVentilation

	// Read meteo from cache
	status.Meteo = p.meteo

	return &status, nil
}

// Settings that are desired.
func (status *Status) NextSettings() Settings {
	/*ventilation := status.Settings.MaxVentilation

	if last.CO2 > 900 {
		ventilation = true
	} else if last.CO2 <= 850 {
		ventilation = false
	}

	// Disable ventilation between 23:00 and 07:00
	if status.Time.Local().Hour() == 23 || status.Time.Local().Hour() < 7 {
		ventilation = false
	}

	// If override file exists, take that
	if payload, err := ioutil.ReadFile(overrideFile); err == nil {
		if payloadInt, err := strconv.Atoi(string(payload)); err == nil {
			ventilation = payloadInt > 0
		}
	}*/

	return Settings{
		HeatingTemperature:  status.Metrics.Temperature(),
		HotWaterTemperature: status.Metrics.HotwaterTemperature(),
		OverrideSensor:      status.Metrics.OverrideSensor(),
		MaxVentilation:      false,
	}
}
