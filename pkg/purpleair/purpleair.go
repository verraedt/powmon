package purpleair

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const PurpleAirReferer = "https://map.purpleair.com"

const PurpleAirToken = "https://api.purpleair.com/token?version=2.0.11"

const PurpleAirSluispark = "https://api.purpleair.com/v1/sensors/7302?fields=name,last_seen,temperature,humidity,pressure,0.3_um_count,0.5_um_count,1.0_um_count,2.5_um_count,5.0_um_count,10.0_um_count,pm1.0_cf_1,pm2.5_cf_1,pm10.0_cf_1,pm1.0_atm,pm2.5_atm,pm10.0_atm"

type PurpleAir struct {
	Sensor Sensor    `json:"sensor"`
	Time   time.Time `json:"-"`
}

//nolint:tagliatelle
type Sensor struct {
	ID                   int     `json:"sensor_index"`
	Label                string  `json:"name"`
	LastSeen             int64   `json:"last_seen"`
	TemperatureFarenheit float64 `json:"temperature"`                                // Present in first sensor only
	Humidity             float64 `json:"humidity"`                                   // Present in first sensor only
	Pressure             float64 `json:"pressure" gauge:"powmon_purpleair_pressure"` // Present in first sensor only
	P03um                float64 `json:"0.3_um_count" gauge:"powmon_purpleair_p_0_3_um"`
	P05um                float64 `json:"0.5_um_count" gauge:"powmon_purpleair_p_0_5_um"`
	P10um                float64 `json:"1.0_um_count" gauge:"powmon_purpleair_p_1_0_um"`
	P25um                float64 `json:"2.5_um_count" gauge:"powmon_purpleair_p_2_5_um"`
	P50um                float64 `json:"5.0_um_count" gauge:"powmon_purpleair_p_5_0_um"`
	P100um               float64 `json:"10.0_um_count" gauge:"powmon_purpleair_p_10_0_um"`
	PM10cf1              float64 `json:"pm1.0_cf_1" gauge:"powmon_purpleair_pm1_0_cf_1"`
	PM25cf1              float64 `json:"pm2.5_cf_1" gauge:"powmon_purpleair_pm2_5_cf_1"`
	PM100cf1             float64 `json:"pm10.0_cf_1" gauge:"powmon_purpleair_pm10_0_cf_1"`
	PM10atm              float64 `json:"pm1.0_atm" gauge:"powmon_purpleair_pm1_0_atm"`
	PM25atm              float64 `json:"pm2.5_atm" gauge:"powmon_purpleair_pm2_5_atm"`
	PM100atm             float64 `json:"pm10.0_atm" gauge:"powmon_purpleair_pm10_0_atm"`
}

func FetchPurpleAirToken() (string, error) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", PurpleAirToken, nil)
	if err != nil {
		return "", err
	}

	req.Header.Set("Referer", PurpleAirReferer)

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	payload, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(payload), nil
}

// ErrTooOld is returned if the found temperature measurement is older than 10 minutes.
var ErrTooOld = errors.New("result is too old")

// ReadPurpleAirURL fetches meteo information from a given URL.
func ReadPurpleAirURL(apiURL string) (PurpleAir, error) {
	result := PurpleAir{}

	token, err := FetchPurpleAirToken()
	if err != nil {
		return result, err
	}

	resp, err := http.Get(apiURL + "&token=" + url.QueryEscape(token)) //nolint:gosec
	if err != nil {
		return result, err
	}

	defer resp.Body.Close()

	payload, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = json.Unmarshal(payload, &result)
	if err != nil {
		return result, err
	}

	result.Time = time.Unix(result.Sensor.LastSeen, 0)

	if time.Since(result.Time) > 15*time.Minute {
		return result, ErrTooOld
	}

	return result, nil
}
