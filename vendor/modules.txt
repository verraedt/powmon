# github.com/beorn7/perks v1.0.0
github.com/beorn7/perks/quantile
# github.com/golang/protobuf v1.3.2
github.com/golang/protobuf/proto
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/konsorten/go-windows-terminal-sequences v1.0.1
github.com/konsorten/go-windows-terminal-sequences
# github.com/matttproud/golang_protobuf_extensions v1.0.1
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/peterverraedt/go-pigpio v0.0.0-20211219174350-974faccb45f8
## explicit
github.com/peterverraedt/go-pigpio
# github.com/prometheus/client_golang v0.9.3
## explicit
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/internal
github.com/prometheus/client_golang/prometheus/promhttp
# github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.4.0
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.0.0-20190507164030-5867b95ac084
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
# github.com/ryotarai/prometheus-query v0.0.0-20181009101647-ea75bc6bc1b1
## explicit
github.com/ryotarai/prometheus-query/client
# github.com/sirupsen/logrus v1.2.0
## explicit
github.com/sirupsen/logrus
# github.com/spf13/cobra v1.1.3
## explicit
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.5
github.com/spf13/pflag
# golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
golang.org/x/crypto/ssh/terminal
# golang.org/x/sys v0.0.0-20190624142023-c5567b49c5d0
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
## explicit
# google.golang.org/appengine v1.6.1
## explicit
